#!/bin/bash

# Start in the directory from where the script is run
start_dir="$(pwd)"

# Function to delete files larger than 100MB
function delete_large_files {
  local dir="$1"
  cd "$dir" || exit
  for file in *; do
    if [ -f "$file" ] && [ $(stat -c %s "$file") -gt 104857600 ]; then
      rm -f "$file"
      echo "Deleted $file"
    elif [ -d "$file" ]; then
      delete_large_files "$file"
    fi
  done
  cd ..
}

# Call the function to start deletion
delete_large_files "$start_dir"
